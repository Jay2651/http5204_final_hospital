﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kirkland_Hospital.Models
{
    public class KirklandContext : DbContext
    {
        public KirklandContext(DbContextOptions<KirklandContext> options) : base(options)
        {
        }
        public DbSet<Doctors> Doctors { get; set; }
        public DbSet<Event> Event { get; set; }
        public DbSet<Volunteer> Volunteer { get; set; }
        public DbSet<UserEvents> UserEvents { get; set; }
        public DbSet<Users> Users { get; set; }
    }
}

