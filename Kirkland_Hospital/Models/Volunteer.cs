﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Kirkland_Hospital.Models
{
    public class Volunteer
    {
        [Key]
        public int Enroll_ID { get; set; }
        [Required, StringLength(255), Display(Name = "Interested Field")]
        public string Interested { get; set; }

        [Required, StringLength(255), Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Required, StringLength(255), Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Required, StringLength(255), Display(Name = "Email")]
        [EmailAddress]
        public string Email_ID { get; set; }
        [Required, StringLength(255), Display(Name = "Address")]
        public string Address { get; set; }
        [Required, StringLength(100), Display(Name = "City")]
        public string City { get; set; }
        [Required, StringLength(255), Display(Name = "Province")]
        public string Province { get; set; }
        [Required, StringLength(100), Display(Name = "Postal Code")]
        public string Postal_Code { get; set; }
        [Required, StringLength(255), Display(Name = "Contact Number")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid phone number")]        
        public string Contact_No { get; set; }
        public bool IsAgree { get; set; }
        public bool IsApproved { get; set; }
        public string Created_By { get; set; }
        public DateTime? Created_Date { get; set; }
        public string Modified_By { get; set; }
        public DateTime? Modified_Date { get; set; }
    }
}
