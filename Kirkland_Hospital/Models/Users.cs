﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Kirkland_Hospital.Models
{
    public class Users
    {
        [Key]
        public int ID { get; set; }
        [Required, StringLength(255), Display(Name = "User Name")]
        public string User_Name { get; set; }
        [Required, StringLength(255), Display(Name = "Password")]
        public string Password { get; set; }
        public bool IsActive { get; set; }
        public string Created_By {get;set;}
        public DateTime? Created_Date { get; set; }
        public string Modified_By { get; set; }
        public DateTime? Modified_Date { get; set; }
    }
}
