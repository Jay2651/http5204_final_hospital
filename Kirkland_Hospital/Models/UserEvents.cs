﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Kirkland_Hospital.Models
{
    public class UserEvents
    {
        [Key]
        public int ID { get; set; }
        [Required, StringLength(255), Display(Name = "User First Name")]
        public string User_First_Name { get; set; }
        [Required, StringLength(255), Display(Name = "Last Name")]
        public string User_Last_Name { get; set; }
        [Required, StringLength(255), Display(Name = "User Email")]
        [EmailAddress]
        public string User_Email { get; set; }
        public int Event_ID { get; set; }
    }
}
