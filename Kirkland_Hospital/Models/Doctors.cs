﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Kirkland_Hospital.Models
{
    public class Doctors
    {
        [Key]
        public int Doctor_ID { get; set; }
        [DisplayName("Doctor Name")]
        [Required, StringLength(100), Display(Name = "Doctor Name")]
        public string Doctor_Name { get; set; }

        [Required, StringLength(255), Display(Name = "Doctor Contact Info")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid phone number")]
        public string Doctor_Contact_info { get; set; }

        [Required, StringLength(255), Display(Name = "Doctor Position")]
        public string Doctor_position { get; set; }
        [Required, StringLength(255), Display(Name = "Available Time")]
        public string Available_Time { get; set; }
        [Required, Display(Name = "Available Date")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Available_Date { get; set; }
        [Required, StringLength(255), Display(Name = "Specialization")]
        public string Specialization { get; set; }
        public string Created_By { get; set; }
        public DateTime? Created_Date { get; set; }
        public string Modified_By { get; set; }
        public DateTime? Modified_Date { get; set; }
    }
}
