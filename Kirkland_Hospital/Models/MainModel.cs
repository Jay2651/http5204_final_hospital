﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Kirkland_Hospital.Models
{
    public class MainModel
    {
    }

    public class LoginViewModel
    {
        [Required, StringLength(100), Display(Name = "User Name")]
        public string UserName { get; set; }

        [Required, StringLength(100), Display(Name = "Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "Remember Me")]
        public bool RememberMe { get; set; }
        public string ReturnUrl { get; set; }

    }


    public class DoctorMainModel
    {
        public DateTime? Date { get; set; }
        public List<Doctors> doctorlist { get; set; }
    }

    public class EventMainModel
    {
        public DateTime? Date { get; set; }
        public List<Event> eventlist { get; set; }
    }


    public class EventModel
    {
        public Event events { get; set; }
        public UserEvents userevent { get; set; }
    }
}
