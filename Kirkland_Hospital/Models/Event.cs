﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Kirkland_Hospital.Models
{
    public class Event
    {
        [Key]
        public int Event_ID { get; set; }
        [Required, StringLength(255), Display(Name = "Event Name")]
        public string Event_Name { get; set; }
        [Required, StringLength(255), Display(Name = "Event Location")]
        public string Event_Location { get; set; }
        [Required,  Display(Name = "Event Date")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Event_Date { get; set; }
        [Required, StringLength(255), Display(Name = "Event Time")]
        public string Event_Time { get; set; }
        
        [Required, Display(Name = "Event Desription")]
        public string Event_Description { get; set; }
        
        public string Created_By { get; set; }
        public DateTime? Created_Date { get; set; }
        public string Modified_By { get; set; }
        public DateTime? Modified_Date { get; set; }
        public string Event_Hyperlink { get; set; }
      
        public string Event_Image { get; set; }
    }
}
