﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Kirkland_Hospital.Migrations
{
    public partial class _14April : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "User_Email",
                table: "Event");

            migrationBuilder.DropColumn(
                name: "User_First_Name",
                table: "Event");

            migrationBuilder.DropColumn(
                name: "User_Last_Name",
                table: "Event");

            migrationBuilder.AddColumn<bool>(
                name: "IsApproved",
                table: "Volunteer",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "UserEvents",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Event_ID = table.Column<int>(nullable: false),
                    User_Email = table.Column<string>(maxLength: 255, nullable: false),
                    User_First_Name = table.Column<string>(maxLength: 255, nullable: false),
                    User_Last_Name = table.Column<string>(maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserEvents", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserEvents");

            migrationBuilder.DropColumn(
                name: "IsApproved",
                table: "Volunteer");

            migrationBuilder.AddColumn<string>(
                name: "User_Email",
                table: "Event",
                maxLength: 255,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "User_First_Name",
                table: "Event",
                maxLength: 255,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "User_Last_Name",
                table: "Event",
                maxLength: 255,
                nullable: false,
                defaultValue: "");
        }
    }
}
