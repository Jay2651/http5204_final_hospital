﻿using Kirkland_Hospital.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Kirkland_Hospital.Controllers
{
    public class AdminController : Controller
    {

        private KirklandContext _context;

        public AdminController(KirklandContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            return View();
        }


        [AllowAnonymous]
        public IActionResult Logout()
        {
            var login = HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Index","Home");
        }

        [HttpGet]
        public IActionResult Login()
        {
            try
            {

            }
            catch (Exception ex)
            {


            }
            return View();
        }


        [HttpPost]
        public IActionResult Login(LoginViewModel model)
        {
            if (!string.IsNullOrEmpty(model.UserName) && string.IsNullOrEmpty(model.Password))
            {
                return RedirectToAction("Login");
            }

            //Check the user name and password  
            //Here can be implemented checking logic from the database  
            Users obj = _context.Users.Where(m => m.User_Name == model.UserName && m.Password == model.Password).FirstOrDefault();
            if (obj!=null)
            {

                //Create the identity for the user  
                var identity = new ClaimsIdentity(new[] {
                    new Claim(ClaimTypes.Name, model.UserName),
                     new Claim(ClaimTypes.Role, "Admin")
                }, CookieAuthenticationDefaults.AuthenticationScheme);

                var principal = new ClaimsPrincipal(identity);
                new AuthenticationProperties
                {
                    IsPersistent = true,
                    ExpiresUtc = DateTime.UtcNow.AddMinutes(20)
                };
                var login = HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);

                return RedirectToAction("Index", "Admin");
            }

            return View();
        }

        #region 
        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Register(Users obj)
        {
            return View();
        }


        #endregion



        #region Doctor
        [HttpGet]
        [Authorize(Roles = "Admin")]        
        public IActionResult DoctorDetails()
        {
            DoctorMainModel obj = new DoctorMainModel();
            try
            {
                obj.Date = null;
                obj.doctorlist = _context.Doctors.ToList();
            }
            catch (Exception ex)
            {

            }
            return View(obj);
        }


        [HttpGet]
        [Authorize(Roles = "Admin")]
        public IActionResult SaveDoctors()
        {
            try
            {
            }
            catch (Exception ex)
            {

            }
            return View();
        }

        [HttpPost]
        public IActionResult SaveDoctors(Doctors obj)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (obj.Doctor_ID == 0)
                    {
                        obj.Created_By = "ADMIN";
                        obj.Created_Date = DateTime.Now;
                        _context.Add(obj);
                        _context.SaveChanges();
                        TempData["Message"] = string.Format("Record Successfully Saved");
                    }
                }
                else
                {
                    return View(obj);
                }
            }
            catch (Exception ex)
            {

            }
            return RedirectToAction("DoctorDetails");
        }


        [HttpGet]
        [Authorize(Roles = "Admin")]
        public IActionResult UpdateDoctors(int? id)
        {
            Doctors obj = new Doctors();
            try
            {
                if (id != null)
                {
                    obj = _context.Doctors.Where(m => m.Doctor_ID == id).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {

            }
            return View(obj);
        }

        [HttpPost]
        public IActionResult UpdateDoctors(Doctors obj)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (obj.Doctor_ID > 0)
                    {
                        obj.Modified_By = "ADMIN";
                        obj.Modified_Date = DateTime.Now;
                        _context.Update(obj);
                        _context.SaveChanges();
                        TempData["Message"] = string.Format("Record Successfully Updated");
                    }
                }
                else
                {
                    return View(obj);
                }
            }
            catch (Exception ex)
            {

            }
            return RedirectToAction("DoctorDetails");
        }

        [Authorize(Roles = "Admin")]
        public IActionResult DeleteDoctors(int? id)
        {
            try
            {
                var doctor = _context.Doctors.Find(id);
                _context.Remove(doctor);
                _context.SaveChanges();
                TempData["Alert"] = string.Format("Record Successfully Deleted");
            }
            catch (Exception ex)
            {

            }
            return RedirectToAction("DoctorDetails");
        }

        #endregion Doctor

        #region Event
        [HttpGet]
        [Authorize(Roles = "Admin")]
        public IActionResult EventDetails()
        {
            List<Event> eventlist = new List<Event>();
            try
            {
                eventlist = _context.Event.ToList();
            }
            catch (Exception ex)
            {

            }
            return View(eventlist);
        }


        [HttpGet]
        [Authorize(Roles = "Admin")]
        public IActionResult SaveEvent()
        {
            try
            {
            }
            catch (Exception ex)
            {

            }
            return View();
        }

        [HttpPost]
        public IActionResult SaveEvent(Event obj)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (obj.Event_ID == 0)
                    {
                        obj.Created_By = "ADMIN";
                        obj.Created_Date = DateTime.Now;
                        _context.Add(obj);
                        _context.SaveChanges();
                        TempData["Message"] = string.Format("Record Successfully Saved");
                    }
                }
                else
                {
                    return View(obj);
                }
            }
            catch (Exception ex)
            {

            }
            return RedirectToAction("EventDetails");
        }


        [HttpGet]
        [Authorize(Roles = "Admin")]
        public IActionResult UpdateEvent(int? id)
        {
            Event obj = new Event();
            try
            {
                if (id != null)
                {
                    obj = _context.Event.Where(m => m.Event_ID == id).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {

            }
            return View(obj);
        }

        [HttpPost]
        public IActionResult UpdateEvent(Event obj)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (obj.Event_ID > 0)
                    {
                        obj.Modified_By = "ADMIN";
                        obj.Modified_Date = DateTime.Now;
                        _context.Update(obj);
                        _context.SaveChanges();
                        TempData["Message"] = string.Format("Record Successfully Updated");
                    }
                }
                else
                {
                    return View(obj);
                }
            }
            catch (Exception ex)
            {

            }
            return RedirectToAction("EventDetails");
        }

        [Authorize(Roles = "Admin")]
        public IActionResult DeleteEvent(int? id)
        {
            try
            {
                var events = _context.Event.Find(id);
                _context.Remove(events);
                _context.SaveChanges();
                TempData["Alert"] = string.Format("Record Successfully Deleted");
            }
            catch (Exception ex)
            {

            }
            return RedirectToAction("EventDetails");
        }
        #endregion Event

        #region Volunteer
        [HttpGet]
        [Authorize(Roles = "Admin")]
        public IActionResult VolunteerDetails()
        {
            List<Volunteer> volunteerlist = new List<Volunteer>();
            try
            {
                volunteerlist = _context.Volunteer.ToList();
            }
            catch (Exception ex)
            {

            }
            return View(volunteerlist);
        }


        [HttpGet]
        [Authorize(Roles = "Admin")]
        public IActionResult UpdateVolunteer(int? id)
        {
            Volunteer obj = new Volunteer();
            try
            {
                if (id != null)
                {
                    obj = _context.Volunteer.Where(m => m.Enroll_ID == id).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {

            }
            return View(obj);
        }

        [HttpPost]
        public IActionResult UpdateVolunteer(Volunteer obj)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (obj.Enroll_ID > 0)
                    {                        
                        obj.Modified_By = "ADMIN";
                        obj.Modified_Date = DateTime.Now;
                        _context.Update(obj);
                        _context.SaveChanges();
                        TempData["Message"] = string.Format("Record Successfully Updated");
                    }
                }
                else
                {
                    return View(obj);
                }
            }
            catch (Exception ex)
            {

            }
            return RedirectToAction("VolunteerDetails");
        }

        [Authorize(Roles = "Admin")]
        public IActionResult DeleteVolunteer(int? id)
        {
            try
            {
                var volunteer = _context.Volunteer.Find(id);
                _context.Remove(volunteer);
                _context.SaveChanges();
                TempData["Alert"] = string.Format("Record Successfully Deleted");
            }
            catch (Exception ex)
            {

            }
            return RedirectToAction("VolunteerDetails");
        }
        #endregion Volunteer

    }
}