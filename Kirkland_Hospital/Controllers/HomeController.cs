﻿using Kirkland_Hospital.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace Kirkland_Hospital.Controllers
{
    public class HomeController : Controller
    {
        private KirklandContext _context;

        public HomeController(KirklandContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult SearchDoctor()
        {
            DoctorMainModel obj = new DoctorMainModel();
            try
            {
                obj.Date = null;
                obj.doctorlist = _context.Doctors.ToList();
            }
            catch (Exception ex)
            {

            }
            return View(obj);
        }

        [HttpPost]
        public IActionResult SearchDoctor(string date)
        {
            DoctorMainModel obj = new DoctorMainModel();
            try
            {
                obj.Date = Convert.ToDateTime(date);
                obj.doctorlist = _context.Doctors.Where(m => m.Available_Date == Convert.ToDateTime(date)).ToList();
            }
            catch (Exception ex)
            {

            }

            return View(obj);
        }



        [HttpGet]
        public IActionResult SearchEvent()
        {
            EventMainModel obj = new EventMainModel();
            try
            {
                obj.Date = null;
                obj.eventlist = _context.Event.ToList();
            }
            catch (Exception ex)
            {

            }
            return View(obj);
        }

        [HttpPost]
        public IActionResult SearchEvent(string date)
        {
            EventMainModel obj = new EventMainModel();
            try
            {
                obj.Date = Convert.ToDateTime(date);
                obj.eventlist = _context.Event.Where(m => m.Event_Date == Convert.ToDateTime(date)).ToList();
            }
            catch (Exception ex)
            {

            }

            return View(obj);
        }



        [HttpGet]
        public IActionResult EventDetails(int? id)
        {
            EventModel obj = new EventModel();
            try
            {
                obj.events = _context.Event.Where(m => m.Event_ID == id).FirstOrDefault();
                obj.userevent = new UserEvents();
            }
            catch (Exception ex)
            {

            }
            return View(obj);
        }

        [HttpPost]
        public IActionResult UserEvent(EventModel obj)
        {
            UserEvents obju = new UserEvents();
            try
            {
                obju.User_First_Name = obj.userevent.User_First_Name;
                obju.User_Last_Name = obj.userevent.User_Last_Name;
                obju.User_Email = obj.userevent.User_Email;
                obju.Event_ID = obj.events.Event_ID;
                _context.Add(obju);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {

            }
            return RedirectToAction("EventDetails", new { id = obj.events.Event_ID });
        }


        [HttpGet]
        public IActionResult Volunteer()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Volunteer(Volunteer obj)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (obj.IsAgree == true)
                    {
                        obj.IsApproved = false;
                        obj.Created_By = "USER";
                        obj.Created_Date = DateTime.Now;
                        _context.Add(obj);
                        _context.SaveChanges();
                        TempData["Message"] = string.Format("Form Successfully Submitted");
                    }
                    else
                    {
                        TempData["Alert"] = string.Format("Please Agree to terms and Conditions");
                        return View(obj);
                    }
                }
                else
                {
                    return View(obj);
                }
            }
            catch (Exception ex)
            {

            }
            return RedirectToAction("Volunteer");
        }

    }
}
